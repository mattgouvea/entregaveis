
class Time:
    def __init__(self, nome, pontos, estado, mascote, serie, titulos):
        self.nome = nome
        self.pontos = pontos
        self.estado = estado
        self.mascote = mascote
        self.serie = serie
        self.titulos = titulos
    
    def __repr__(self):
        return "Nome do time: {}\n".format(self.nome) + "Total de Pontos: {}".format(self.pontos)

    def __gt__(self, outro_time):
        return self.pontos > outro_time.pontos

    def __lt__(self, outro_time):
        return self.pontos < outro_time.pontos

    def __eq__(self, outro_time):
        return self.pontos == outro_time.pontos

    def __sub__(self, outro_time):
        return self.pontos - outro_time.pontos
    
    def __add__(self, outro_time):
        return self.pontos + outro_time.pontos
